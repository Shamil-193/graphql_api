import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { map, Observable } from 'rxjs';
import { Category } from '../interfaces/category';
import { Query } from '../interfaces/query';


@Injectable()
export class CategoryService {
  constructor(private apollo: Apollo) { }

  getProductsCategories(): Observable<Category[]> {
    return this.apollo.query<Query>(
      {
        query: gql`query Query
        {
          products{
            items{
              slug
              }
          }
        }`
      })
      .pipe(
        map(({ data: { products: { items } } }) => items)
      );
  }

}
