import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { map, Observable } from 'rxjs';
import { TypeFilter } from '../interfaces/filter';
import { ProductList, Product } from '../interfaces/product';
import { Query } from '../interfaces/query';


@Injectable()
export class ProductService {
  private filterName: string = '';
  private filterCategory: string[] = [];

  constructor(private apollo: Apollo) { }

  set filterNameWrapped(name: string) {
    this.filterName = name;
  }

  set filterCategoryWrapped(category: string[]) {
    this.filterCategory = category;
  }

  getProductList(page = 1, limit = 5): Observable<ProductList> {
    const filterType: string = this.filterCategory.length ? TypeFilter.IN : TypeFilter.NOTIN;

    return this.apollo.query<Query>(
      {
        query: gql`query Query
        {
          products(options:
          {
            skip: ${page * limit - limit}
            take: ${limit} 
            filter:{
              name:{contains:${JSON.stringify(this.filterName)}}
              slug:{${filterType}${JSON.stringify(this.filterCategory)}}
            }
          }){
            totalItems
            items{
              name
              id
              createdAt
              updatedAt
              languageCode
              description
              slug
            }
          }
        }`
      }
    )
      .pipe(
        map(({ data: { products } }) => products)
      );
  }

  getProductById(id: string): Observable<Product> {
    return this.apollo.query<Query>(
      {
        query: gql`query Query
        {
          products(options:
            {filter:
              {id:
                {eq:${JSON.stringify(id)}}
              }
              }){
              totalItems
              items{
                name
                id
                createdAt
                updatedAt
                languageCode
                description
                slug
            }
          }
        }`
      })
      .pipe(
        map(({ data: { products: { items } } }) => items[0])
      );
  }

}
