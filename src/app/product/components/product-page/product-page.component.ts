import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { first, Subject, takeUntil } from 'rxjs';
import { Observable, switchMap } from 'rxjs';
import { parameterNameList } from '../../interfaces/items-list';
import { Product } from '../../interfaces/product';
import { ProductService } from '../../services/product.service';


@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductPageComponent {
  protected componentDestroy$: Subject<void> = new Subject<void>();

  product$: Observable<Product> = this.route.params.pipe(
    first(),
    switchMap((params: Params) => this.productService.getProductById(params['id']))
  );

  parameterNameList = parameterNameList;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute
  ) { }

}
