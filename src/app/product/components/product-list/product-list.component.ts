import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../../interfaces/category';
import { ProductList } from '../../interfaces/product';
import { CategoryService } from '../../services/category.service';
import { ProductService } from '../../services/product.service';


@Component({
  selector: 'app-product-page',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductListComponent implements OnDestroy {

  productList$: Observable<ProductList> = this.productService.getProductList();
  productsCategories$: Observable<Category[]> = this.categoryService.getProductsCategories();

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
  ) { }

  ngOnDestroy(): void {
    this.clearFilter();
  }

  changePage(page: number): void {
    this.productList$ = this.productService.getProductList(page);
  }

  nameFilterChanged(name: string): void {
    this.productService.filterNameWrapped = name;
    this.productList$ = this.productService.getProductList();
  }

  categoriesFilterChanged(category: string[]): void {
    this.productService.filterCategoryWrapped = category;
    this.productList$ = this.productService.getProductList();
  }

  clearFilter(): void {
    this.productService.filterCategoryWrapped = [];
    this.productService.filterNameWrapped = '';
  }

}

