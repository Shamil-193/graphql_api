import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, filter, takeUntil, tap } from 'rxjs';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Category } from '../../interfaces/category';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterComponent implements OnInit, OnDestroy {
  @Input() categories: Category[];
  @Output() onNameFilterChanged = new EventEmitter<string>();
  @Output() onCategoriesFilterChanged = new EventEmitter<string[]>();

  protected componentDestroy$: Subject<void> = new Subject<void>();

  form: FormGroup = new FormGroup({
    name: new FormControl(null),
    categories: new FormControl(null),
  });

  ngOnInit(): void {
    this.initFilterName();
    this.initFilterCategories();
  }

  ngOnDestroy(): void {
    this.saveFilter();
    this.componentDestroy$.next();
    this.componentDestroy$.complete();
  }

  initFilterName(): void {
    this.form.get('name').valueChanges.pipe(
      takeUntil(this.componentDestroy$),
      filter(data => data !== null),
      debounceTime(350),
      tap((nameFilter: string) => {
        this.onNameFilterChanged.emit(nameFilter)
        StorageService.setData('filter', { nameFilter: this.form.get('name').value, categoryFilter: this.form.get('categories').value })
      })
    ).subscribe();

    if (StorageService.getData('filter')) {
      this.form.get('name').setValue(StorageService.getData('filter').nameFilter);
    }
  }

  initFilterCategories(): void {
    this.form.get('categories').valueChanges.pipe(
      takeUntil(this.componentDestroy$),
      filter(data => data !== null),
      debounceTime(350),
      tap((categoryFilter: string[]) => {
        this.onCategoriesFilterChanged.emit(categoryFilter)
        StorageService.setData('filter', { nameFilter: this.form.get('name').value, categoryFilter })
      })
    ).subscribe();

    if (StorageService.getData('filter')) {
      this.form.get('categories').setValue(StorageService.getData('filter')?.categoryFilter);
    }
  }

  saveFilter(): void {
    StorageService.setData('filter', { nameFilter: this.form.get('name').value, categoryFilter: this.form.get('categories').value });
  }

}
