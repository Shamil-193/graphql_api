export interface ProductList {
    totalItems: number
    items: Product[]
}

export interface Product {
    name: string,
    id: string,
    createdAt: Date,
    updatedAt: Date,
    languageCode: string,
    description: string,
    slug: string
}