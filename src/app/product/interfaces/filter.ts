export interface Filter {
    nameFilter: string | null,
    categoryFilter: string[] | null,
}

export enum TypeFilter {
    IN = 'in:',
    NOTIN = 'notIn:',
}