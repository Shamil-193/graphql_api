export const parameterNameList = {
    "createdAt": "Создано",
    "languageCode": "Язык",
    "id": "id",
    "name": "Наименование",
    "updatedAt": "Обновлено",
    "description": "Описание",
    "slug": "Категория"
}