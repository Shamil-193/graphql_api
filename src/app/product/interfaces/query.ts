import { ProductList } from "./product"

export type Query = {
    products: ProductList
}
