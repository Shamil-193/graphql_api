import { Injectable } from '@angular/core';
import { Filter } from 'src/app/product/interfaces/filter';

@Injectable()
export class StorageService {
  static setData( key: string, data: any): void {
    JSON.stringify(localStorage.setItem(key,JSON.stringify(data)));
  }

  static getData(key: string): Filter | null {
    return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : null;
  }
}
