import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent {
  currentPage: number = 1;

  @Input() total: number;
  @Output() onChangePage = new EventEmitter<number>();

  changePage(page: number): void {
    if (page >= 1) {
      this.currentPage = page;
      this.onChangePage.emit(this.currentPage);
    }
  }

}
