import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { PaginationComponent } from './components/pagination/pagination.component';
import { StorageService } from './services/storage.service';


@NgModule({
  declarations: [
    PaginationComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule
  ],
  exports: [
    PaginationComponent,
  ],
  providers: [StorageService],
})
export class SharedModule { }
